package net.ihe.gazelle.goc.scoring.actiontest;
import java.util.Random;
final public class MatrixViewer {
	private  int size1;             // number of rows
	private  int size2;             // number of columns
	private  double[][] data;   // M-by-N array

	// create size1-by-size2 matrix of 0's
	public MatrixViewer(int size1, int size2) {
		this.size1 = size1;
		this.size2 = size2;
		data = new double[size1][size2];
	}

	// create matrix based on 2d array
	public MatrixViewer(double[][] data) {
		size1 = data.length;
		size2 = data[0].length;
		this.data = new double[size1][size2];
		for (int i = 0; i < size1; i++)
			for (int j = 0; j < size2; j++)
				this.data[i][j] = data[i][j];
	}

	public static String toString(double[][] M) {
	    String separator = ", ";
	    StringBuffer result = new StringBuffer();

	    // iterate over the first dimension
	    for (int i = 0; i < M.length; i++) {
	        // iterate over the second dimension
	        for(int j = 0; j < M[i].length; j++){
	            result.append(M[i][j]);
	            result.append(separator);
	        }
	        // remove the last separator
	        result.setLength(result.length() - separator.length());
	        // add a line break.
	        result.append("\n");
	    }
	    return result.toString();
	}
}