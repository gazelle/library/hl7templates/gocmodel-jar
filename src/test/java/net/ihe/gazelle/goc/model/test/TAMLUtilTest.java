package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.goc.template.definer.model.Taml;
import net.ihe.gazelle.goc.uml.utils.TAMLUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class TAMLUtilTest {

	@Test
	public void testCreateTAML() {
		Taml taml = TAMLUtil.createTAML("base", "lab", "ccda");
		assertTrue(taml != null); 
		assertTrue(taml.getBaseConstraint().equals("base"));
		assertTrue(taml.getIDs().equals("lab"));
		assertTrue(taml.getTargetIDScheme().equals("ccda"));
	}

}
