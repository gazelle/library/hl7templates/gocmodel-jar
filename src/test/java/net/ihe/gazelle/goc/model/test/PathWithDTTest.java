package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.*;

import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.PathWithDT;

public class PathWithDTTest {

	@Test
	public void testHashCode() {
		PathWithDT pp = new PathWithDT();
		pp.setElementDT("aa");
		pp.setElementName("oo");
		PathWithDT pp2 = new PathWithDT();
		pp2.setElementDT("aa");
		pp2.setElementName("oo");
		assertTrue(pp2.hashCode() == pp.hashCode());
		
	}

	@Test
	public void testEqualsObject() {
		PathWithDT pp = new PathWithDT();
		pp.setElementDT("aa");
		pp.setElementName("oo");
		PathWithDT pp2 = new PathWithDT();
		pp2.setElementDT("aa");
		pp2.setElementName("oo");
		assertTrue(pp.equals(pp2));
	}

	@Test
	public void testPathWithDT() {
		PathWithDT pp = new PathWithDT();
		PathWithDT pp2 = new PathWithDT();
		assertTrue(pp.equals(pp2));
	}

	@Test
	public void testPathWithDTString() {
		PathWithDT pp = new PathWithDT("aa");
		PathWithDT pp2 = new PathWithDT("aa");
		assertTrue(pp.equals(pp2));
	}

	@Test
	public void testPathWithDTStringString() {
		PathWithDT pp = new PathWithDT("aa", "oo");
		PathWithDT pp2 = new PathWithDT("aa", "oo");
		assertTrue(pp.equals(pp2));
	}
	
	@Test
	public void testToString() throws Exception {
		PathWithDT pp = new PathWithDT("aa", "oo");
		assertTrue(pp.toString().equals("PDT[aa, oo]"));
	}

}
