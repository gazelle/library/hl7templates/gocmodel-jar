package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.assertTrue;
import net.ihe.gazelle.goc.uml.Model;
import net.ihe.gazelle.goc.uml.utils.ElementImportUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.ElementImport;
import net.ihe.gazelle.goc.xmm.ImportedElement;

import org.junit.Test;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ElementImportUtilTest {

	@Test
	public void testFindElementImport1() {
		XMI xmi = new XMI();
		xmi.setModel(new Model());
		xmi.getModel().getElementImport().add(new ElementImport());
		xmi.getModel().getElementImport().get(0).setImportedElement(new ImportedElement());
		xmi.getModel().getElementImport().get(0).getImportedElement().setHref("aa#_CuDm3a70EeGxJei_o6JmIA");
		xmi.getModel().getElementImport().add(new ElementImport());
		xmi.getModel().getElementImport().get(1).setImportedElement(new ImportedElement());
		xmi.getModel().getElementImport().get(1).getImportedElement().setHref("aa#aaaa");
		ElementImport aa = ElementImportUtil.findElementImport(xmi.getModel(), "ADXP");
		assertTrue(aa != null);
		ElementImport bb = ElementImportUtil.findElementImport(xmi.getModel(), "AD");
		assertTrue(bb == null);
	}
	
	@Test
	public void testFindElementImport2() {
		XMI xmi = new XMI();
		xmi.setModel(new Model());
		xmi.getModel().getElementImport().add(new ElementImport());
		xmi.getModel().getElementImport().get(0).setImportedElement(new ImportedElement());
		xmi.getModel().getElementImport().get(0).getImportedElement().setHref("aa#_DFqOha70EeGxJei_o6JmIA");
		xmi.getModel().getElementImport().add(new ElementImport());
		xmi.getModel().getElementImport().get(1).setImportedElement(new ImportedElement());
		xmi.getModel().getElementImport().get(1).getImportedElement().setHref("aa#aaaa");
		ElementImport aa = ElementImportUtil.findElementImport(xmi.getModel(), "CalendarCycle");
		assertTrue(aa != null);
		ElementImport bb = ElementImportUtil.findElementImport(xmi.getModel(), "AD");
		assertTrue(bb == null);
	}

}
