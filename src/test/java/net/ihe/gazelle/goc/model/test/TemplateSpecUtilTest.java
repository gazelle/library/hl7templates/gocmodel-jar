package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.assertTrue;

import net.ihe.gazelle.goc.template.definer.model.TemplateSpec;
import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.uml.utils.TemplateSpecUtil;
import net.ihe.gazelle.goc.xmm.PackagedElement;

import org.junit.Test;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class TemplateSpecUtilTest {

	@Test
	public void testCreateTemplateSpec1() {
		PackagedElement pe = PackagedElementUtil.initPackagedElement();
		TemplateSpec tt = TemplateSpecUtil.createTemplateSpec(pe, "1.2.3", "templateId.root");
		assertTrue(tt.getBaseClass().equals(pe.getId()));
		assertTrue(tt.getId().equals("1.2.3"));
		assertTrue(tt.getPath().equals("templateId.root"));
	}
	
	@Test
	public void testCreateTemplateSpec2() {
		PackagedElement pe = PackagedElementUtil.initPackagedElement();
		TemplateSpec tt = TemplateSpecUtil.createTemplateSpec(pe, "1.2.3", "typeId");
		assertTrue(tt.getBaseClass().equals(pe.getId()));
		assertTrue(tt.getId().equals("1.2.3"));
		assertTrue(tt.getPath().equals("typeId"));
	}

}
