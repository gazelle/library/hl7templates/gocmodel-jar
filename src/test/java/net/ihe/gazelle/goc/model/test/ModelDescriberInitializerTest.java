package net.ihe.gazelle.goc.model.test;

import net.ihe.gazelle.goc.uml.utils.ModelDescriberInitializer;
import net.ihe.gazelle.goc.umlmodel.model.ModelDescriber;
import net.ihe.gazelle.goc.umlmodel.util.ModelDescriberUtil;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class ModelDescriberInitializerTest {

	@Test
	public void testInitializeListModelDescriberString1() {
		String pathResources = "src/test/resources/testModels1.xml";
		List<ModelDescriber> aa = ModelDescriberInitializer.initializeListModelDescriber(pathResources);
		assertTrue(aa.size() == 5);
		
		ModelDescriber md = ModelDescriberUtil.getModelDescriberByIdentifier("common", aa);
		assertTrue(md.getIdentifier().equals("common"));
		assertTrue(md.getPath().contains("/common-models/models/common.uml"));
		assertTrue(md.getRelativeXMIPath().equals("../../common-models/models/common.uml"));
		assertTrue(md.getModelUML().getName().equals("CommonClasses"));
		assertTrue(md.getListTemplateIdentifierUMLId().size()==0);
		assertTrue(md.getMapListUMLElementsToUMLId().get("CommonOperationsStatic") != null);
		
		md = ModelDescriberUtil.getModelDescriberByIdentifier("datatypes", aa);
		assertTrue(md.getIdentifier().equals("datatypes"));
		assertTrue(md.getPath().contains("/datatypes-model/models/datatypes.uml"));
		assertTrue(md.getRelativeXMIPath().equals("../../datatypes-model/models/datatypes.uml"));
		assertTrue(md.getModelUML().getName().equals("datatypes"));
		assertTrue(md.getListTemplateIdentifierUMLId().size()==1);
		assertTrue(md.getMapListUMLElementsToUMLId().get("AD") != null);
	}
	
	@Test
	public void testInitializeListModelDescriberString2() {
		String pathResources = "src/test/resources/testModels2.xml";
		List<ModelDescriber> aa = ModelDescriberInitializer.initializeListModelDescriber(pathResources);
		assertTrue(aa.size()==1);
		assertTrue(aa.get(0).getIdentifier().equals("medication"));
		assertTrue(aa.get(0).getMapListUMLElementsToUMLId().get("EntityClassMaterial").equals("_VzV5mM86EeGVX9h3ivlQvw"));
	}

}
