package net.ihe.gazelle.goc.scripts;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.PackageableElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.goc.uml.utils.CDAClassesMap;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ExtractChildDT {
	
	private static Logger log = LoggerFactory.getLogger(ExtractChildDT.class);
	
	public static void main(String[] args) {
		Set<String> lchild = new HashSet<String>();
		//String[] ll = new String[] {"BL", "BN", "CR", "PQ", "PQR", "INT", "MO", "REAL", "URL", "TS"};
		String[] ll = new String[] {"AD", "BIN", "EN"};
		for (String string : ll) {
			extractListChildDT(string, lchild);
		}
		for (String string : lchild) {
			log.info("\"" + string + "\", ");
		}
	}

	private static void extractListChildDT(String type, Set<String> lchild) {
		EList<PackageableElement> pe = CDAClassesMap.getPackagedElements("datatypes");
		for (PackageableElement packageableElement : pe) {
			if (packageableElement instanceof org.eclipse.uml2.uml.Class){
				org.eclipse.uml2.uml.Class clas = (org.eclipse.uml2.uml.Class)packageableElement;
				for (Generalization gen: clas.getGeneralizations()) {
					if (gen.getGeneral() instanceof Class){
						String parentName = ((Class)gen.getGeneral()).getName();
						if (parentName.equals(type)){
							lchild.add(packageableElement.getName());
							extractListChildDT(packageableElement.getName(), lchild);
						}
					}
				}
				for (Classifier gen: clas.getGenerals()) {
					if (gen instanceof Class){
						String parentName = ((Class)gen).getName();
						if (parentName.equals(type)){
							lchild.add(packageableElement.getName());
							extractListChildDT(packageableElement.getName(), lchild);
						}
					}
				}
			}
		}
	}

}
