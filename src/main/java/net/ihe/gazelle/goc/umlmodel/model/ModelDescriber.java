package net.ihe.gazelle.goc.umlmodel.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.uml2.uml.Model;

/**
 * <p>ModelDescriber class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public class ModelDescriber {
	
	private Model modelUML;
	
	private String identifier;
	
	private String path;
	
	private String relativeXMIPath;
	
	private Map<String, String> mapListUMLElementsToUMLId = new HashMap<>();
	
	private List<String> listTemplateIdentifierUMLId;

	/**
	 * <p>Getter for the field <code>listTemplateIdentifierUMLId</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<String> getListTemplateIdentifierUMLId() {
		return listTemplateIdentifierUMLId;
	}

	/**
	 * <p>Setter for the field <code>listTemplateIdentifierUMLId</code>.</p>
	 *
	 * @param listTemplateIdentifierUMLId a {@link java.util.List} object.
	 */
	public void setListTemplateIdentifierUMLId(List<String> listTemplateIdentifierUMLId) {
		this.listTemplateIdentifierUMLId = listTemplateIdentifierUMLId;
	}

	/**
	 * <p>Getter for the field <code>modelUML</code>.</p>
	 *
	 * @return a {@link org.eclipse.uml2.uml.Model} object.
	 */
	public Model getModelUML() {
		return modelUML;
	}

	/**
	 * <p>Setter for the field <code>modelUML</code>.</p>
	 *
	 * @param modelUML a {@link org.eclipse.uml2.uml.Model} object.
	 */
	public void setModelUML(Model modelUML) {
		this.modelUML = modelUML;
	}

	/**
	 * <p>Getter for the field <code>path</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPath() {
		return path;
	}

	/**
	 * <p>Setter for the field <code>path</code>.</p>
	 *
	 * @param path a {@link java.lang.String} object.
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * <p>Getter for the field <code>relativeXMIPath</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getRelativeXMIPath() {
		return relativeXMIPath;
	}

	/**
	 * <p>Setter for the field <code>relativeXMIPath</code>.</p>
	 *
	 * @param relativeXMIPath a {@link java.lang.String} object.
	 */
	public void setRelativeXMIPath(String relativeXMIPath) {
		this.relativeXMIPath = relativeXMIPath;
	}

	/**
	 * <p>Getter for the field <code>identifier</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * <p>Setter for the field <code>identifier</code>.</p>
	 *
	 * @param identifier a {@link java.lang.String} object.
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * <p>Getter for the field <code>mapListUMLElementsToUMLId</code>.</p>
	 *
	 * @return a {@link java.util.Map} object.
	 */
	public Map<String, String> getMapListUMLElementsToUMLId() {
		return mapListUMLElementsToUMLId;
	}

	/**
	 * <p>Setter for the field <code>mapListUMLElementsToUMLId</code>.</p>
	 *
	 * @param mapListUMLElementsToUMLId a {@link java.util.Map} object.
	 */
	public void setMapListUMLElementsToUMLId(
			Map<String, String> mapListUMLElementsToUMLId) {
		this.mapListUMLElementsToUMLId = mapListUMLElementsToUMLId;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((identifier == null) ? 0 : identifier.hashCode());
		result = prime * result
				+ ((relativeXMIPath == null) ? 0 : relativeXMIPath.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ModelDescriber other = (ModelDescriber) obj;
		if (identifier == null) {
			if (other.identifier != null) {
				return false;
			}
		} else if (!identifier.equals(other.identifier)) {
			return false;
		}
		if (relativeXMIPath == null) {
			if (other.relativeXMIPath != null) {
				return false;
			}
		} else if (!relativeXMIPath.equals(other.relativeXMIPath)) {
			return false;
		}
		if (path == null) {
			if (other.path != null) {
				return false;
			}
		} else if (!path.equals(other.path)) {
			return false;
		}
		return true;
	}
	
}
