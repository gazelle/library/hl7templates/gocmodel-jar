package net.ihe.gazelle.goc.umlmodel.util;

import java.util.List;

import net.ihe.gazelle.goc.umlmodel.model.ModelDescriber;

/**
 * <p>ModelDescriberUtil class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class ModelDescriberUtil {
	
	private ModelDescriberUtil() {}
	
	/**
	 * <p>getModelDescriberByIdentifier.</p>
	 *
	 * @param identifier a {@link java.lang.String} object.
	 * @param listModelDescriber a {@link java.util.List} object.
	 * @return a {@link net.ihe.gazelle.goc.umlmodel.model.ModelDescriber} object.
	 */
	public static ModelDescriber getModelDescriberByIdentifier(String identifier, List<ModelDescriber> listModelDescriber) {
		if (listModelDescriber != null) {
			for (ModelDescriber modelDescriber : listModelDescriber) {
				if (modelDescriber.getIdentifier() != null && modelDescriber.getIdentifier().equals(identifier)) {
					return modelDescriber;
				}
			}
		}
		return null;
	}

}
