package net.ihe.gazelle.goc.scripts;

import net.ihe.gazelle.goc.template.definer.model.ConstraintType;
import net.ihe.gazelle.goc.template.definer.model.Taml;
import net.ihe.gazelle.goc.uml.Model;
import net.ihe.gazelle.goc.uml.utils.GOCModelMarshaller;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.oasis.xsd.*;

import org.apache.commons.lang.StringUtils;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>AssertionGenerator class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class AssertionGenerator {

    private static Logger log = LoggerFactory.getLogger(AssertionGenerator.class);

    private static final String LIST_PACK = "listPack";

    private static final String IDENTIFIER = "identifier";

    private static Map<String, String> mapParagraphToPage = new HashMap<>();

    private static Map<String, String> mapOwnedRuleToTAML = new TreeMap<>();

    private static int id = 1;

    private static String projectDist = "";

    private static List<String> listpackagesToBeProcessed = new ArrayList<>();

    private static Set<String> listSections = new TreeSet<>();


    static {
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("src/main/resources/assertions.properties"));
        } catch (Exception e) {
            log.error("not able to load properties", e);
        }
        for (Object key : prop.keySet()) {
            if (key instanceof String) {
                String skey = (String) key;
                if (!skey.equals(IDENTIFIER) && !skey.equals(LIST_PACK)) {
                    mapParagraphToPage.put(skey, (String) prop.get(skey));
                } else if (skey.equals(IDENTIFIER)) {
                    projectDist = (String) prop.get(skey);
                } else if (skey.equals(LIST_PACK)) {
                    StringTokenizer stringTokenizer = new StringTokenizer((String) prop.get(skey), ",");
                    while (stringTokenizer.hasMoreElements()) {
                        String el = stringTokenizer.nextToken();
                        if (StringUtils.trimToNull(el) != null) {
                            listpackagesToBeProcessed.add(el.trim());
                        }
                    }
                }
            }
        }
    }

    private AssertionGenerator() {
    }

    private static String generateAssertionID() {
        String res = projectDist;
        String formatted = String.format("%03d", id++);
        res = res + "-" + formatted;
        return res;
    }

    /**
     * <p>main.</p>
     *
     * @param args an array of {@link java.lang.String} objects.
     */
    public static void main(String[] args) {
        try {
            XMI xmi = (XMI) GOCModelMarshaller.readGOCObject(new FileInputStream(
                    "/Users/epoiseau/IdeaProjects/cdaEpsosSpec-model/models/CDAEpsosSpec.uml"));

            String gen = generateAssertionsFromXMI(xmi);
            log.info("gen = {}", gen);
            for (Entry<String, String> entry : mapOwnedRuleToTAML.entrySet()) {
                log.info("<TemplateDefiner:taml xmi:id=\"{}\" base_Constraint=\"{}\" IDs=\"{}\" targetIDScheme=\"{}\"/>",
                        new Object[]{EcoreUtil.generateUUID(), entry.getKey(), entry.getValue(), projectDist}
                );
            }
        } catch (FileNotFoundException | JAXBException e) {
            log.error("Error : ", e);
        }
    }

    /**
     * <p>generateAssertionsFromXMI.</p>
     *
     * @param xmi a {@link net.ihe.gazelle.goc.xmi.XMI} object.
     * @return a {@link java.lang.String} object.
     * @throws javax.xml.bind.JAXBException if any.
     */
    public static String generateAssertionsFromXMI(XMI xmi) throws JAXBException {
        if (xmi != null) {
            TestAssertionSetType tas = new TestAssertionSetType();
            tas.setCommon(createCommonType());
            if (xmi.getModel() != null) {
                parseEpackageForAssertions(xmi.getModel(), xmi.getTaml(), xmi.getConstraintType(), tas);
            }
            ObjectFactory of = new ObjectFactory();
            JAXBElement<TestAssertionSetType> aa = of.createTestAssertionSet(tas);
            JAXBContext jc = JAXBContext.newInstance(TestAssertionSetType.class);
            Marshaller mar = jc.createMarshaller();
            mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            mar.marshal(aa, baos);
            return baos.toString();
        }

        return null;
    }

    private static void parseEpackageForAssertions(Model model, List<Taml> listTAML, List<ConstraintType> listCT, TestAssertionSetType tas) {
        if (model != null && tas != null && model.getPackagedElement() != null) {
            parsePackageElementsForAssertions(model.getPackagedElement(), listTAML, listCT, tas);
        }
    }

    private static void parsePackageElementsForAssertions(List<PackagedElement> packagedElements, List<Taml> listTAML,
                                                          List<ConstraintType> listCT, TestAssertionSetType tas) {
        if (packagedElements != null) {
            for (PackagedElement pe : packagedElements) {
                processPackageElement(listTAML, listCT, tas, pe);
            }
        }
    }

    private static void processPackageElement(List<Taml> listTAML,
                                              List<ConstraintType> listCT, TestAssertionSetType tas, PackagedElement pe) {
        if (pe != null) {
            if (pe.getType().equals("uml:Package")) {
                if (listpackagesToBeProcessedAllowToProcessPE(pe)) {
                    List<PackagedElement> sub = pe.getPackagedElements();
                    if (sub != null) {
                        parsePackageElementsForAssertions(sub, listTAML, listCT, tas);
                    }
                }
            } else {
                for (OwnedRule ownedRule : pe.getOwnedRule()) {
                    updateAssertionByOwnedRule(ownedRule, listTAML, listCT, tas);
                }
            }
        }
    }

    private static boolean listpackagesToBeProcessedAllowToProcessPE(PackagedElement pe) {
        return (pe != null) && (listpackagesToBeProcessed.isEmpty() || listpackagesToBeProcessed.contains(pe.getName()));
    }

    private static void updateAssertionByOwnedRule(OwnedRule ownedRule, List<Taml> listTAML, List<ConstraintType> listCT, TestAssertionSetType tas) {
        if (ownedRule != null && tas != null && !listTAMLContainsOwnedRuleID(listTAML, ownedRule)) {
            TestAssertionType tat = new TestAssertionType();
            tat.setId(generateAssertionID());
            tat.setPredicate(new PredicateType());
            tat.getPredicate().setLg("NATURAL");
            tat.getPredicate().getContent().add(ownedRule.getOwnedComment().getBody());
            tat.setPrescription(new PrescriptionType());
            tat.getPrescription().setLevel(extractPrescriptionLevel(ownedRule, listCT));
            TagType ttdsection = createTagTypeSection(ownedRule.getOwnedComment().getBody());
            if (ttdsection != null) {
                tat.getTag().add(ttdsection);
                TagType ttdpage = createTagTypePage(ttdsection.getValue());
                if (ttdpage != null) {
                    tat.getTag().add(ttdpage);
                }
            }
            tas.getTestAssertion().add(tat);
            mapOwnedRuleToTAML.put(ownedRule.getId(), tat.getId());
        }
    }

    private static TagType createTagTypePage(String section) {
        String page = mapParagraphToPage.get(section);
        if (page != null && !page.trim().equals("")) {
            if (!page.matches("\\d+")) {
                log.error("problem to process section : {}", section);
            } else {
                TagType tt = new TagType();
                tt.setTname("Page");
                tt.setValue(page);
                return tt;
            }
        } else {
            log.error("no section : {}", section);
        }
        return null;
    }

    private static TagType createTagTypeSection(String body) {
        if (body != null) {
            Pattern pat = Pattern.compile(".*\\(.*, (.*)\\)");
            Matcher mat = pat.matcher(body);
            if (mat.find()) {
                String section = mat.group(1).trim();
                if (section.contains("R")) {
                    section = "10";
                }
                TagType tt = new TagType();
                tt.setTname("Section");
                tt.setValue(section);
                listSections.add(section);
                return tt;
            } else {
                log.error("section not found : {}", body);
            }
        }
        return null;
    }

    private static String extractPrescriptionLevel(OwnedRule ownedRule, List<ConstraintType> listCT) {
        String res = "mandatory";
        if (listCT != null) {
            for (ConstraintType constraintType : listCT) {
                if (constraintType.getBaseConstraint() != null && constraintType.getBaseConstraint().equals(ownedRule.getId()) && constraintType.getType() != null) {
                    switch (constraintType.getType()) {
                        case "WARNING":
                            res = "preferred";
                            break;
                        case "ERROR":
                            res = "mandatory";
                            break;
                        case "INFO":
                            res = "permitted";
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        return res;
    }

    private static boolean listTAMLContainsOwnedRuleID(List<Taml> listTAML, OwnedRule ownedRule) {
        boolean res = false;
        if (listTAML != null) {
            for (Taml taml : listTAML) {
                if (taml.getBaseConstraint().contains(ownedRule.getId())) {
                    res = true;
                }
            }
        }
        return res;
    }

    private static CommonType createCommonType() {
        CommonType ct = new CommonType();
        ObjectFactory of = new ObjectFactory();
        NormativeSourceType nst = createNormativeSource();
        JAXBElement<NormativeSourceType> aa = of.createCommonTypeNormativeSource(nst);
        ct.getContent().add(aa);
        return ct;
    }

    private static NormativeSourceType createNormativeSource() {
        NormativeSourceType nst = new NormativeSourceType();
        nst.getContent().add(new RefSourceItemType());
        return nst;
    }

}
