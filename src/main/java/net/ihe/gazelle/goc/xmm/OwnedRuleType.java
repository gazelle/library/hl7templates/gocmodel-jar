package net.ihe.gazelle.goc.xmm;

/**
 * <p>OwnedRuleKind class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public enum OwnedRuleType {
	
	ERROR("ERROR"), WARNING("WARNING"), INFO("INFO");
	
	private String value;
	
	private OwnedRuleType(String value) {
		this.value = value;
	}
	
	/**
	 * <p>Getter for the field <code>value</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getValue() {
		return value;
	}
	
}
