package net.ihe.gazelle.goc.uml.utils;

import net.ihe.gazelle.goc.template.definer.model.TemplateSpec;
import net.ihe.gazelle.goc.xmm.PackagedElement;

/**
 * <p>TemplateSpecUtil class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class TemplateSpecUtil {
	
	private TemplateSpecUtil() {}
	
	/**
	 * <p>createTemplateSpec.</p>
	 *
	 * @param pe a {@link net.ihe.gazelle.goc.xmm.PackagedElement} object.
	 * @param templateId a {@link java.lang.String} object.
	 * @param templateIdPath a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.goc.template.definer.model.TemplateSpec} object.
	 */
	public static TemplateSpec createTemplateSpec(PackagedElement pe, String templateId, String templateIdPath) {
		TemplateSpec ts = new TemplateSpec();
		ts.setId(templateId);
		ts.setPath(templateIdPath);
		ts.setBaseClass(pe.getId());
		return ts;
	}

}
