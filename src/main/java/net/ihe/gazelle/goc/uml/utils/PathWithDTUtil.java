package net.ihe.gazelle.goc.uml.utils;

import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * <p>PathWithDTUtil class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class PathWithDTUtil {
	
	private PathWithDTUtil() {}
	
	/**
	 * <p>extractPathFromListPathWithDT.</p>
	 *
	 * @param lpwdt a {@link java.util.List} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String extractPathFromListPathWithDT(List<PathWithDT> lpwdt) {
		if (lpwdt != null) {
			StringBuilder sb = new StringBuilder("");
			int i = 0;
			for (PathWithDT pathWithDT : lpwdt) {
				if (i++>0) {
					sb.append(".");
				}
				sb.append(pathWithDT.getElementName());
			}
			return sb.toString();
		}
		return "";
	}
	
	/**
	 * <p>extractTypedPathFromListPathWithDT.</p>
	 *
	 * @param lpwdt a {@link java.util.List} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String extractTypedPathFromListPathWithDT(List<PathWithDT> lpwdt) {
		if (lpwdt != null) {
			StringBuilder sb = new StringBuilder("");
			int i = 0;
			for (PathWithDT pathWithDT : lpwdt) {
				if (i++>0) {
					sb.append(".");
				}
				if (StringUtils.trimToNull(pathWithDT.getElementDT()) != null) {
					sb = new StringBuilder(pathWithDT.getElementDT());
				}
				else {
					sb.append(pathWithDT.getElementName());
				}
			}
			return sb.toString();
		}
		return "";
	}
	
	/**
	 * we ignore the last DT because we want to know the real type of this element for example
	 *
	 * @param lpwdt a {@link java.util.List} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String extractTypedPathFromListPathWithDTWithoutLastDTChecking(List<PathWithDT> lpwdt) {
		if (lpwdt != null) {
			StringBuilder sb = new StringBuilder("");
			int i = 0;
			for (PathWithDT pathWithDT : lpwdt) {
				if (i++>0) {
					sb.append(".");
				}
				if (StringUtils.trimToNull(pathWithDT.getElementDT()) != null && i<lpwdt.size()) {
					sb = new StringBuilder(pathWithDT.getElementDT());
				}
				else {
					sb.append(pathWithDT.getElementName());
				}
			}
			return sb.toString();
		}
		return "";
	}

}
