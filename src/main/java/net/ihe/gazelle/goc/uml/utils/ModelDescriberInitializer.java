package net.ihe.gazelle.goc.uml.utils;

import net.ihe.gazelle.goc.umlmodel.model.ModelDescriber;
import net.ihe.gazelle.goc.umlmodel.model.ModelUMLDesc;
import net.ihe.gazelle.goc.umlmodel.model.ModelsDefinition;
import net.ihe.gazelle.goc.umlmodel.util.ModelsDefinitionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>ModelDescriberInitializer class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class ModelDescriberInitializer {

	private static final Logger LOG = LoggerFactory.getLogger(ModelDescriberInitializer.class);
	
	private ModelDescriberInitializer() {
		// private constructor
	}
	
	static List<ModelDescriber> initializeListModelDescriber() {
		String pathResources = System.getProperty("MODELS_DESCRIBER",System.getProperty("HL7TEMP_RESOURCES_PATH", "../hl7templates-resources") + File.separator + 
					System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic") + 
					"/modelsDefinition.xml");
		LOG.info("modelDefinition path = {}", pathResources);
		return initializeListModelDescriber(pathResources);
	}

	/**
	 * <p>initializeListModelDescriber.</p>
	 *
	 * @param pathResources a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public static List<ModelDescriber> initializeListModelDescriber(String pathResources) {
		ModelsDefinition md = ModelsDefinitionUtil.unmarshallModelsDefinition(pathResources);
		List<ModelDescriber> res = new ArrayList<>();
		for (ModelUMLDesc modelUMLDesc : md.getUMLModelsDescription().getModelUMLDesc()) {
			ModelDescriber desc = new ModelDescriber();
			desc.setIdentifier(modelUMLDesc.getIdentifier());
			desc.setPath(modelUMLDesc.getPath());
			desc.setRelativeXMIPath(modelUMLDesc.getRelativeXMIPath());
			desc.setModelUML(UMLLoader.load(desc.getPath()));
			desc.setMapListUMLElementsToUMLId(
					CDAClassesMap.initializeMapFromEListPackageableElements(desc.getMapListUMLElementsToUMLId(), 
							desc.getModelUML().getPackagedElements()));
			desc.setListTemplateIdentifierUMLId(modelUMLDesc.getTemplateIdentifierUMLID());
			res.add(desc);
		}
		return res;
	}

}
