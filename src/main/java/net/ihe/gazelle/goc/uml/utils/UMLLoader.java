package net.ihe.gazelle.goc.uml.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory.Registry;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>UMLLoader class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class UMLLoader {

    private static final String E_CLASS = "EClass";

    private static Logger log = LoggerFactory.getLogger(UMLLoader.class);

    private static final String LITERAL = "literal";
    private static final String EENUMLITERAL = "EEnumLiteral";

    private UMLLoader() {
    }


    /**
     * <p>isEnumeration.</p>
     *
     * @param pathProperty a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static boolean isEnumeration(String pathProperty) {
        int i = pathProperty.indexOf('.');
        if (i >= 0) {
            String firstTypeClass = pathProperty.substring(0, i);
            String second = "" + pathProperty.substring(i + 1);
            PackageableElement pe = lookForPackageableElementFromName(firstTypeClass);
            if (pe instanceof org.eclipse.uml2.uml.Class) {
                Class clas = (Class) pe;
                return isEnumeration(clas, second);
            }
            return false;
        } else {
            PackageableElement cdaenum = lookForPackageableElementFromName(pathProperty);
            return cdaenum instanceof Enumeration;
        }
    }

    /**
     * <p>isUMLClass.</p>
     *
     * @param pathProperty a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static boolean isUMLClass(String pathProperty) {
        int i = pathProperty.indexOf('.');
        if (i >= 0) {
            String firstTypeClass = pathProperty.substring(0, i);
            String second = "" + pathProperty.substring(i + 1);
            PackageableElement pe = lookForPackageableElementFromName(firstTypeClass);
            if (pe instanceof org.eclipse.uml2.uml.Class) {
                Class clas = (Class) pe;
                return isUMLClass(clas, second);
            }
            return false;
        } else {
            PackageableElement cdaenum = lookForPackageableElementFromName(pathProperty);
            return cdaenum instanceof Class;
        }
    }

    private static boolean isUMLClass(Class clase, String attrName) {
        List<Property> oa = getGeneralOwnedAttribute(clase);
        if (attrName.indexOf('.') < 0) {
            for (Property property : oa) {
                if (property.getName().equals(attrName)) {
                    return property.getType() instanceof Class;
                }
            }
        } else {
            for (Property property : oa) {
                if (property.getType() instanceof Class) {
                    Class prop = (Class) property.getType();
                    String firstName = attrName.indexOf('.') == (-1) ? attrName : attrName.substring(0, attrName.indexOf('.'));
                    if (property.getName().equals(firstName)) {
                        return isUMLClass(prop, attrName.substring(attrName.indexOf('.') + 1));
                    }
                }
            }
        }
        return false;
    }

    /**
     * <p>checkIfEnumerationContainsValue.</p>
     *
     * @param enumType a {@link java.lang.String} object.
     * @param value    a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static boolean checkIfEnumerationContainsValue(String enumType, String value) {
        PackageableElement cdaenum = lookForPackageableElementFromName(enumType);
        if (!(cdaenum instanceof Enumeration)) {
            log.error("The supposed enumeration is not realy one ! : " + enumType + ", " + value);
            return false;
        }
        Enumeration enn = (Enumeration) cdaenum;
        for (EnumerationLiteral el : enn.getOwnedLiterals()) {
            if (UmlServices.hasStereotype(el, EENUMLITERAL) &&
                    UmlServices.getStereotypeValue(el, EENUMLITERAL, LITERAL) != null &&
                    !((String) UmlServices.getStereotypeValue(el, EENUMLITERAL, LITERAL)).trim().equals("")) {
                if (((String) UmlServices.getStereotypeValue(el, EENUMLITERAL, LITERAL)).equals(value)) {
                    return true;
                }
            } else {
                if (el.getName().equals(value)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * <p>getUMLNameForLitteral.</p>
     *
     * @param enumType a {@link java.lang.String} object.
     * @param value    a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getUMLNameForLitteral(String enumType, String value) {
        PackageableElement cdaenum = lookForPackageableElementFromName(enumType);
        if (!(cdaenum instanceof Enumeration)) {
            log.error("The supposed enumeration is not realy one ! : " + enumType + ", " + value);
            return null;
        }
        Enumeration enn = (Enumeration) cdaenum;
        for (EnumerationLiteral el : enn.getOwnedLiterals()) {
            if (UmlServices.hasStereotype(el, EENUMLITERAL) &&
                    UmlServices.getStereotypeValue(el, EENUMLITERAL, LITERAL) != null &&
                    !((String) UmlServices.getStereotypeValue(el, EENUMLITERAL, LITERAL)).trim().equals("")) {
                if (((String) UmlServices.getStereotypeValue(el, EENUMLITERAL, LITERAL)).equals(value)) {
                    return el.getName();
                }
            } else {
                if (el.getName().equals(value)) {
                    return el.getName();
                }
            }
        }
        return null;
    }

    /**
     * <p>lookForPackageableElementFromName.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @return a {@link org.eclipse.uml2.uml.PackageableElement} object.
     */
    public static PackageableElement lookForPackageableElementFromName(String name) {
        return CDAClassesMap.lookForPackageableElementFromName(name);
    }

    /**
     * <p>lookForPackageableElementFromName.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @param packageName a string
     * @return a {@link org.eclipse.uml2.uml.PackageableElement} object.
     */
    public static PackageableElement lookForPackageableElementFromName(String name, String packageName) {
        return CDAClassesMap.lookForPackageableElementFromName(name, packageName);
    }

    private static boolean isEnumeration(Class clase, String attrName) {
        List<Property> oa = getGeneralOwnedAttribute(clase);
        if (attrName.indexOf('.') < 0) {
            for (Property property : oa) {
                if (property.getName().equals(attrName)) {
                    return property.getType() instanceof Enumeration;
                }
            }
        } else {
            for (Property property : oa) {
                if (property.getType() instanceof Class) {
                    Class prop = (Class) property.getType();
                    String firstName = attrName.indexOf('.') == (-1) ? attrName : attrName.substring(0, attrName.indexOf('.'));
                    if (property.getName().equals(firstName)) {
                        return isEnumeration(prop, attrName.substring(attrName.indexOf('.') + 1));
                    }
                }
            }
        }
        return false;
    }

    /**
     * <p>getPropertyTypeName.</p>
     *
     * @param pathProperty a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getPropertyTypeName(String pathProperty) {
        int i = pathProperty.indexOf('.');
        if (i >= 0) {
            String firstTypeClass = pathProperty.substring(0, i);
            String second = pathProperty.substring(i + 1);
            PackageableElement pe = lookForPackageableElementFromName(firstTypeClass);
            if (pe instanceof org.eclipse.uml2.uml.Class) {
                Class clas = (Class) pe;
                return getPropertyTypeName(clas, second);
            }
        } else {
            PackageableElement pe = lookForPackageableElementFromName(pathProperty);
            if (pe instanceof org.eclipse.uml2.uml.Class) {
                return pathProperty;
            }
        }
        return null;
    }

    /**
     * <p>getListPropertiesNamesWithoutEAttributes.</p>
     *
     * @param classType a {@link java.lang.String} object.
     * @return a {@link java.util.Set} object.
     */
    public static Set<String> getListPropertiesNamesWithoutEAttributes(String classType) {
        Set<String> res = new TreeSet<>();
        PackageableElement pe = lookForPackageableElementFromName(classType);
        if (pe instanceof org.eclipse.uml2.uml.Class) {
            Class clas = (Class) pe;
            for (Property prop : clas.getAllAttributes()) {
                if (!UmlServices.hasStereotype(prop, "ParentProperty") && !UmlServices.hasStereotype(prop, "EAttribute")) {
                    res.add(prop.getName());
                }
            }
        }
        return res;
    }

    /**
     * <p>getListPropertiesNames.</p>
     *
     * @param classType a {@link java.lang.String} object.
     * @return a {@link java.util.Set} object.
     */
    public static Set<String> getListPropertiesNames(String classType) {
        Set<String> res = new TreeSet<>();
        PackageableElement pe = lookForPackageableElementFromName(classType);
        if (pe instanceof org.eclipse.uml2.uml.Class) {
            Class clas = (Class) pe;
            for (Property prop : clas.getAllAttributes()) {
                if (!UmlServices.hasStereotype(prop, "ParentProperty")) {
                    res.add(prop.getName());
                }
            }
        }
        return res;
    }

    private static String getPropertyTypeName(Class clase, String attrName) {
        List<Property> oa = getGeneralOwnedAttribute(clase);
        if (attrName.indexOf('.') < 0) {
            for (Property property : oa) {
                if (property.getName().equals(attrName)) {
                    if (property.getType().getName() != null) {
                        return property.getType().getName();
                    } else {
                        if (property.getType() instanceof org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl) {
                            org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl primType =
									(org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl) property.getType();
                            return primType.eProxyURI().fragment();
                        } else {
                            log.info("Error in processing : The attribute has no typeName and no primitiveType name : " + attrName +
                                    "," + property.getName());
                        }
                        return null;
                    }
                }
            }
        } else {
            for (Property property : oa) {
                if (property.getType() instanceof Class) {
                    Class prop = (Class) property.getType();
                    String firstName = attrName.indexOf('.') == (-1) ? attrName : attrName.substring(0, attrName.indexOf('.'));
                    if (property.getName().equals(firstName)) {
                        return getPropertyTypeName(prop, attrName.substring(attrName.indexOf('.') + 1));
                    }
                }
            }
        }
        return null;
    }

    /**
     * <p>getMaxAttribute.</p>
     *
     * @param javaPath a {@link java.lang.String} object.
     * @return a int.
     */
    public static int getMaxAttribute(String javaPath) {
        int lastIndex = javaPath.lastIndexOf('.');
        String first = javaPath.substring(0, lastIndex);
        String last = javaPath.substring(lastIndex + 1);
        return getMaxAttribute(first, last);
    }

    /**
     * <p>getMaxAttribute.</p>
     *
     * @param typeNameParam a {@link java.lang.String} object.
     * @param attrName      a {@link java.lang.String} object.
     * @return a int.
     */
    public static int getMaxAttribute(String typeNameParam, String attrName) {
        String typeName = typeNameParam;
        if (typeName == null) {
            return 0;
        }
        typeName = typeName + "." + attrName;
        int i = typeName.indexOf('.');
        String firstTypeClass = typeName;
        String second = "";

        if (i >= 0) {
            firstTypeClass = typeName.substring(0, i);
            second = typeName.substring(i + 1);
        }
        PackageableElement pe = lookForPackageableElementFromName(firstTypeClass);
        if (pe instanceof org.eclipse.uml2.uml.Class) {
            Class clas = (Class) pe;
            return getMaxAttribute(clas, second);
        }
        return 0;
    }

    /**
     * <p>getMinAttribute.</p>
     *
     * @param typeNameParam a {@link java.lang.String} object.
     * @param attrName      a {@link java.lang.String} object.
     * @return a int.
     */
    public static int getMinAttribute(String typeNameParam, String attrName) {
        String typeName = typeNameParam;
        if (typeName == null) {
            return 0;
        }
        typeName = typeName + "." + attrName;
        int i = typeName.indexOf('.');
        String firstTypeClass = typeName;
        String second = "";

        if (i >= 0) {
            firstTypeClass = typeName.substring(0, i);
            second = typeName.substring(i + 1);
        }
        PackageableElement pe = lookForPackageableElementFromName(firstTypeClass);
        if (pe instanceof org.eclipse.uml2.uml.Class) {
            Class clas = (Class) pe;
            return getMinAttribute(clas, second);
        }
        return 0;
    }

    private static int getMinAttribute(Class clase, String attrName) {
        List<Property> oa = getGeneralOwnedAttribute(clase);
        if (attrName.indexOf('.') < 0) {
            for (Property property : oa) {
                if (property.getName().equals(attrName)) {
                    return property.lowerBound();
                }
            }
        } else {
            for (Property property : oa) {
                if (property.getType() instanceof Class) {
                    Class prop = (Class) property.getType();
                    String firstName = attrName.indexOf('.') == (-1) ? attrName : attrName.substring(0, attrName.indexOf('.'));
                    if (property.getName().equals(firstName)) {
                        return getMinAttribute(prop, attrName.substring(attrName.indexOf('.') + 1));
                    }
                }
            }
        }
        return 0;
    }

    private static int getMaxAttribute(Class clase, String attrName) {
        List<Property> oa = getGeneralOwnedAttribute(clase);
        if (attrName.indexOf('.') < 0) {
            for (Property property : oa) {
                if (property.getName().equals(attrName)) {
                    return property.upperBound();
                }
            }
        } else {
            for (Property property : oa) {
                if (property.getType() instanceof Class) {
                    Class prop = (Class) property.getType();
                    String firstName = attrName.indexOf('.') == (-1) ? attrName : attrName.substring(0, attrName.indexOf('.'));
                    if (property.getName().equals(firstName)) {
                        return getMaxAttribute(prop, attrName.substring(attrName.indexOf('.') + 1));
                    }
                }
            }
        }
        return 0;
    }

    private static List<Property> getGeneralOwnedAttribute(Class clase) {
        List<Property> res = new ArrayList<>(clase.getAttributes());
        for (Generalization gen : clase.getGeneralizations()) {
            if (gen.getGeneral() instanceof Class) {
                res.addAll(getGeneralOwnedAttribute((Class) gen.getGeneral()));
            }
        }
        for (Classifier gen : clase.getGenerals()) {
            if (gen instanceof Class) {
                res.addAll(getGeneralOwnedAttribute((Class) gen));
            }
        }

        return res;
    }

    private static List<Class> getGeneralClasses(Class clase) {
        List<Class> res = new ArrayList<>();
        for (Generalization gen : clase.getGeneralizations()) {
            if (gen.getGeneral() instanceof Class) {
                res.add((Class) gen.getGeneral());
                res.addAll(getGeneralClasses((Class) gen.getGeneral()));
            }
        }
        for (Classifier gen : clase.getGenerals()) {
            if (gen instanceof Class) {
                res.add((Class) gen);
                res.addAll(getGeneralClasses((Class) gen));
            }
        }
        return res;
    }


    /**
     * <p>load.</p>
     *
     * @param umlString a {@link java.lang.String} object.
     * @return a org$eclipse$uml2$uml$Model object.
     */
    protected static org.eclipse.uml2.uml.Model load(String umlString) {
        // the following line is to add the resources Ecore.profile.uml to the path of stereotypes as the value pathmap://UML_PROFILES/
        // is used in the models (datatypes.uml, etc)
        log.info(System.getProperty("HL7TEMP_RESOURCES_PATH", "../hl7templates-resources"));
        URIConverter.URI_MAP.put(URI.createURI("pathmap://UML_PROFILES/"),
                URI.createURI(System.getProperty("HL7TEMP_RESOURCES_PATH", "../hl7templates-resources") + "/uml/profiles/"));
        ResourceSet resourceSet = new ResourceSetImpl();
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("uml", new XMIResourceFactoryImpl());
        resourceSet.getPackageRegistry().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
        resourceSet.getResourceFactoryRegistry();
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
        EPackage.Registry.INSTANCE.put(umlString, UMLPackage.eINSTANCE);
        URI uri = URI.createFileURI(umlString);
        Resource resource = resourceSet.createResource(uri);
        try {
            resource.load(null);
            log.info("Model: load complete");
        } catch (IOException e) {
            log.info("UMLLoader::load: error to load model, ", e);
        }
        Model m = (Model) EcoreUtil.getObjectByType(resource.getContents(), UMLPackage.Literals.MODEL);
        log.info("model name = " + m.getName());
        return m;
    }

    /**
     * <p>verifyTheParentOfType1IsType2.</p>
     *
     * @param type1 a {@link java.lang.String} object.
     * @param type2 a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static boolean verifyTheParentOfType1IsType2(String type1, String type2) {
        if (type2 == null || type1 == null) {
            return false;
        }
        PackageableElement type1PE = lookForPackageableElementFromName(type1);
        if (type1PE == null) {
            return false;
        }
        if (!(type1PE instanceof Class)) {
            return false;
        }
        List<Class> lp = getGeneralClasses((Class) type1PE);
        for (Class cl : lp) {
            if (cl.getName().equals(type2)) {
                return true;
            }
        }
        return false;
    }

    /**
     * <p>getDefaultValue.</p>
     *
     * @param elementType a {@link java.lang.String} object.
     * @param name        a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getDefaultValue(String elementType, String name) {
        if (elementType == null) {
            return null;
        }
        PackageableElement pe = lookForPackageableElementFromName(elementType);
        if (pe instanceof org.eclipse.uml2.uml.Class) {
            org.eclipse.uml2.uml.Class claa = (org.eclipse.uml2.uml.Class) pe;
            EList<Property> oa = claa.getOwnedAttributes();
            Property prop = null;
            for (Property property : oa) {
                if (property.getName().equals(name)) {
                    prop = property;
                }
            }
            if (prop != null && prop.getDefaultValue() != null && prop.getDefaultValue().stringValue() != null) {
                return prop.getDefaultValue().stringValue();
            }
        }
        return null;
    }

    /**
     * <p>datatypeHasMixedElements.</p>
     *
     * @param type a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static boolean datatypeHasMixedElements(String type) {
        if (type != null) {
            PackageableElement pe = CDAClassesMap.lookForPackageableElementFromName(type);
            if (pe == null) {
                return false;
            }
            boolean datatypeContainsMixedContent = UmlServices.hasStereotype(pe, E_CLASS) &&
                    UmlServices.getStereotypeValue(pe, E_CLASS, "xmlContentKind") != null &&
                    ((EnumerationLiteral) UmlServices.getStereotypeValue(pe, E_CLASS, "xmlContentKind")).getName().equals("Mixed");
            if (datatypeContainsMixedContent) {
                return true;
            }
            if (pe instanceof org.eclipse.uml2.uml.Class) {
                org.eclipse.uml2.uml.Class cl = (org.eclipse.uml2.uml.Class) pe;
                return umlClassHasMixedElements(cl);
            }
        }
        return false;
    }


    private static boolean umlClassHasMixedElements(org.eclipse.uml2.uml.Class cl) {
        if (cl.getGeneralizations() != null) {
            for (Generalization clas : cl.getGeneralizations()) {
                boolean containsmix = datatypeHasMixedElements(clas.getGeneral().getName());
                if (containsmix) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * <p>isAnAttribute.</p>
     *
     * @param pathProperty a {@link java.lang.String} object.
     * @return a {@link java.lang.Boolean} object.
     */
    public static Boolean isAnAttribute(String pathProperty) {
        if (pathProperty == null) {
            return false;
        }
        int i = pathProperty.indexOf('.');

        if (i >= 0) {
            String firstTypeClass = pathProperty.substring(0, i);
            String second = pathProperty.substring(i + 1);
            PackageableElement pe = lookForPackageableElementFromName(firstTypeClass);
            if (pe instanceof org.eclipse.uml2.uml.Class) {
                Class clas = (Class) pe;
                return isAnAttribute(clas, second);
            }
            return false;
        }
        return false;
    }

    private static boolean isAnAttribute(Class clase, String attrName) {
        List<Property> oa = getGeneralOwnedAttribute(clase);
        if (attrName.indexOf('.') < 0) {
            for (Property property : oa) {
                if (property.getName().equals(attrName)) {
                    return UmlServices.hasStereotype(property, "EAttribute");
                }
            }
        } else {
            for (Property property : oa) {
                if (property.getType() instanceof Class) {
                    Class prop = (Class) property.getType();
                    String firstName = attrName.indexOf('.') == (-1) ? attrName : attrName.substring(0, attrName.indexOf('.'));
                    if (property.getName().equals(firstName)) {
                        return isAnAttribute(prop, attrName.substring(attrName.indexOf('.') + 1));
                    }
                }
            }
        }
        return false;
    }

}
