package net.ihe.gazelle.goc.uml.utils;

import java.util.List;

import net.ihe.gazelle.goc.ecore.EPackage;
import net.ihe.gazelle.goc.uml.Model;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.AppliedProfile;
import net.ihe.gazelle.goc.xmm.EAnnotations;
import net.ihe.gazelle.goc.xmm.ElementImport;
import net.ihe.gazelle.goc.xmm.ProfileApplication;
import net.ihe.gazelle.goc.xmm.References;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <p>XMIUtil class.</p>
 *
 * @author Abderrazek Boufahja
 * cannot be set to final, extended by test class.
 */
public class XMIUtil {
	
	/**
	 * <p>createXMI.</p>
	 *
	 * @param name a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.goc.xmi.XMI} object.
	 */
	public static XMI createXMI(String name){
		XMI xmi = new XMI();
		Model model = new Model();
		model.setId(EcoreUtil.generateUUID());
		model.setName(name);
		xmi.setModel(model);
		xmi.setEpackage(new EPackage());
		xmi.getEpackage().setBasePackage(model.getId());
		xmi.getEpackage().setPackageName("net.ihe.gazelle." + name);
		
		xmi.getModel().getProfileApplication().add(createEcoreProfileApplication());
		xmi.getModel().getProfileApplication().add(createGOCProfileApplication());
		List<String> elementIIRefs = CDAClassesMap.getIIXMIMappingForElementImport();
		if (elementIIRefs != null && !elementIIRefs.isEmpty()) {
			for (String elementIIRef : elementIIRefs) {
				ElementImport ei = new ElementImport();
				ei.setId(EcoreUtil.generateUUID());
				ei.setImportedElement(new net.ihe.gazelle.goc.xmm.ImportedElement());
				ei.getImportedElement().setType("uml:Class");
				ei.getImportedElement().setHref(elementIIRef);
				xmi.getModel().getElementImport().add(ei);
			}
		}
		return xmi;
	}

	/**
	 * <p>createEcoreProfileApplication.</p>
	 *
	 * @return a {@link net.ihe.gazelle.goc.xmm.ProfileApplication} object.
	 */
	protected static ProfileApplication createEcoreProfileApplication() {
		ProfileApplication prof = new ProfileApplication();
		prof.setId(EcoreUtil.generateUUID());
		prof.setEAnnotations(new EAnnotations());
		prof.getEAnnotations().setId(EcoreUtil.generateUUID());
		prof.getEAnnotations().setSource("http://www.eclipse.org/uml2/2.0.0/UML");
		prof.getEAnnotations().setReferences(new References());
		prof.getEAnnotations().getReferences().setHref("pathmap://UML_PROFILES/Ecore.profile.uml#_z1OFcHjqEdy8S4Cr8Rc_NA");
		prof.getEAnnotations().getReferences().setType("ecore:EPackage");
		prof.setAppliedProfile(new AppliedProfile());
		prof.getAppliedProfile().setHref("pathmap://UML_PROFILES/Ecore.profile.uml#_0");
		return prof;
	}
	
	/**
	 * <p>createGOCProfileApplication.</p>
	 *
	 * @return a {@link net.ihe.gazelle.goc.xmm.ProfileApplication} object.
	 */
	protected static ProfileApplication createGOCProfileApplication() {
		ProfileApplication prof = new ProfileApplication();
		prof.setId(EcoreUtil.generateUUID());
		prof.setEAnnotations(new EAnnotations());
		prof.getEAnnotations().setId(EcoreUtil.generateUUID());
		prof.getEAnnotations().setSource("http://www.eclipse.org/uml2/2.0.0/UML");
		prof.getEAnnotations().setReferences(new References());
		prof.getEAnnotations().getReferences().setHref("../../common-models/models/common-profile.uml#_L1HqYLk9EeadT_oVto_uPg");
		prof.getEAnnotations().getReferences().setType("ecore:EPackage");
		prof.setAppliedProfile(new AppliedProfile());
		prof.getAppliedProfile().setHref("../../common-models/models/common-profile.uml#_1P9GAG7eEeGRT9uCX2f8pg");
		return prof;
	}

}
