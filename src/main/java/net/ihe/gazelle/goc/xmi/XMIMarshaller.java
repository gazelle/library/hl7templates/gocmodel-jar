package net.ihe.gazelle.goc.xmi;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.goc.template.definer.model.ConstraintKind;
import net.ihe.gazelle.goc.template.definer.model.ConstraintType;
import net.ihe.gazelle.goc.template.definer.model.TemplateSpec;
import net.ihe.gazelle.goc.uml.Model;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.PackagedElement;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>XMIMarshaller class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class XMIMarshaller {


    private static Logger log = LoggerFactory.getLogger(XMIMarshaller.class);

    private XMIMarshaller() {
    }

    // NO_UCD (use private)

    /**
     * <p>loadXMI.</p>
     *
     * @param is a {@link java.io.InputStream} object.
     * @return a {@link net.ihe.gazelle.goc.xmi.XMI} object.
     * @throws javax.xml.bind.JAXBException if any.
     */
    public static XMI loadXMI(InputStream is) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(XMI.class);
        Unmarshaller u = jc.createUnmarshaller();
        return (XMI) u.unmarshal(is);
    }

    /**
     * <p>printXMI.</p>
     *
     * @param xmi a {@link net.ihe.gazelle.goc.xmi.XMI} object.
     * @param os  a {@link java.io.OutputStream} object.
     * @throws javax.xml.bind.JAXBException if any.
     */
    public static void printXMI(XMI xmi, OutputStream os) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(XMI.class);
        beforeMarshal(xmi);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(xmi, os);
    }

    private static void beforeMarshal(XMI xmi) {
        if (xmi != null && xmi.getModel() != null && xmi.getConstraintKind().isEmpty()) {
            for (PackagedElement pe : xmi.getModel().getPackagedElement()) {
                for (OwnedRule orul : pe.getOwnedRule()) {
                    addPossibleRuleType(orul, xmi);
                    addPossibleRuleKind(orul, xmi);
                }
            }
        }
    }

    private static void addPossibleRuleType(OwnedRule orul, XMI xmi) {
        if (orul.getOwnedRuleType() != null) {
            ConstraintType ck = new ConstraintType();
            ck.setBaseConstraint(orul.getId());
            ck.setType(orul.getOwnedRuleType().getValue());
            xmi.getConstraintType().add(ck);
        }
    }

    private static void addPossibleRuleKind(OwnedRule orul, XMI xmi) {
        if (orul.getOwnedRuleKind() != null) {
            ConstraintKind ck = new ConstraintKind();
            ck.setBaseConstraint(orul.getId());
            ck.setKind(orul.getOwnedRuleKind().getValue());
            xmi.getConstraintKind().add(ck);
        }
    }

    /**
     * <p>main.</p>
     *
     * @param args an array of {@link java.lang.String} objects.
     */
    public static void main(String[] args) {
        try {
            XMI xmi = new XMI();
            TemplateSpec ts = new TemplateSpec();
            ts.setId(EcoreUtil.generateUUID());
            xmi.getTemplateSpec().add(ts);
            Model model = new Model();
            xmi.setModel(model);
            PackagedElement pe = new PackagedElement();
            model.getPackagedElement().add(pe);
            pe.setId("test");

            XMI xmi2 = loadXMI(new FileInputStream("/Users/epoiseau/IdeaProjects/nblock-model/models/nblock.uml"));

            for (PackagedElement packagedElement : xmi2.getModel().getPackagedElement()) {
                if (packagedElement.getType() != null && packagedElement.getType().equals("uml:Class")) {
                    log.info("mapListNBlockToUMLId.put(\"" + packagedElement.getName() + "\", \"" + packagedElement.getId() + "\");");
                }
            }
        } catch (JAXBException | FileNotFoundException e) {
            log.error("Error : ", e);
        }

    }

}
